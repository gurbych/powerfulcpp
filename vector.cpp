#include <iostream>
#include <cmath>

class Vector {
public:
    double x;
    double y;

    void vectorIncrement(Vector& c) {
        this->x += c.x;
        this->y += c.y;
    }

    void vectorDecrement(Vector& d) {
        this->x -= d.x;
        this->y -= d.y;
    }

    Vector vectorSum(Vector& b) {
        Vector sum;

        sum.x = this->x + b.x;
        sum.y = this->y + b.y;
        return sum;
    }

    Vector vectorDiff(Vector& b) {
        Vector dif;

        dif.x = this->x - b.x;
        dif.y = this->y - b.y;
        return dif;
    }

    double vectorModule() {
        return hypot(this->x, this->y);
    }

    double vectorLength(Vector& b) {
        return hypot(this->x - b.x, this->y - b.y);
    }

    void vectorPrint() {
        std::cout << '(' << this->x << "," << this->y << ')'<< std::endl;
    }

}; 

int main () {
    Vector a = {3, 4};
    Vector b = {1, 1};
    Vector c, d;
    double module, length;

    a.vectorPrint();
    b.vectorPrint();
    //a.vectorIncrement(b);
    //a.vectorPrint();
    //a.vectorDecrement(b);
    //a.vectorPrint();
    //c = a.vectorSum(b);
    //c.vectorPrint();

    //d = a.vectorDiff(b);
    //d.vectorPrint();

    module = a.vectorModule();
    std::cout << module << std::endl;

    length = a.vectorLength(b);
    std::cout << length << std::endl;

    return 0;
}
