#include <stdio.h>
#include <iostream>
#include <cmath>

class Vector {
private:
    double x, y;
public:
    Vector (double pointX, double pointY) {
        x = pointX;
        y = pointY;
    };

    void vectorIncrement(const Vector& other) {
        this->x += other.x;
        this->y += other.y;
    }

    void vectorDecrement(const Vector& other) {
        this->x -= other.x;
        this->y -= other.y;
    }

    bool vectorEqual(const Vector& other) {
        return this->x == other.x && this->y == other.y;
    }

    Vector vectorSum(const Vector& other) {
        Vector sum;
    
        sum.x = this->x + other.x;
        sum.y = this->y + other.y;
        return sum;
    }

    Vector vectorDiff(const Vector& other) {
        Vector dif;
    
        dif.x = this->x - other.x;
        dif.y = this->y - other.y;
    return dif;
    }

    double vectorLen() {
        return hypot(this->x, this->y);
    }

    void vectorPrint() {
        printf("(%g, %g)", this->x, this->y);
    }
};

int main() {
    Vector a (3, 4);
    Vector b (1, 2);
    Vector c, d;
    double module, length;

    a.vectorPrint();
    b.vectorPrint();
    return 0;
}

