#include "List.h"

int main() {
    List lst;

    for (int i = 0; i < 100; i+=10) {
        lst.push_back(i);
    }
    lst.erase(4);
    lst.insert(100, 4);
    std::cout << lst.find(34) << '\n';
    lst.sort();
    int size = lst.size();
    for ( int i = 0; i < size; i++ ) {
        std::cout << lst.pop_back() << ' ';
    }
    //std::cout << lst.pop_back() << std::endl;
    //std::cout << lst.size() << std::endl;
    //std::cout << lst.max_size() << std::endl;

    return 0;
}
