#include "List.h"

List::List(int capacity, double multiplier) {
    this->capacity = capacity;
    this->multiplier = multiplier;
    this->current = 0;
    array = (int*)malloc(capacity*sizeof(int));

    if ( array == NULL ) {
        throw OutOfMemoryException();
    }
}

List::~List() {
    free(array);
}

int List::size() const {
    return current;
}

int List::max_size() const {
    return capacity;
}

void List::erase(int index) {
    if ( index == this->current ) {
        current -= 1;
    } else if ( this->current - index > 0 ) {
        for ( ; index < this->current; index++ ) {
            this->array[index] = this->array[index+1];
        }
        current -= 1;
    }
}

void List::insert(int value, int index) {
    if ( current - index >= 0  ) {
        push_back(0);
        for ( int i = current; i > index; i-- ) {
            array[i] = array[i-1];
        }
        array[index] = value;
    }
}

int List::find(int value) const {
    for ( int i = 0; i < this->size(); i++ ) {
        if ( array[i] == value ) {
            return i;
        }
    }
    return -1;
}

void List::push_back(int value) {
    int newCurrent = current + 1;

    if ( newCurrent > capacity ) {
        int newCapacity = capacity * multiplier;
        int* newArray = (int*)realloc(array, newCapacity * sizeof(int));

        if ( newArray == NULL ) {
            throw OutOfMemoryException();
        }

        capacity = newCapacity;
        array = newArray;
    }

    array[current] = value;
    current = newCurrent;
}

int List::pop_back() {
    if ( current == 0 ) {
        throw ZeroLenException();
    }
    current -= 1;
    return array[current];
}

void List::sort() {
    for ( int i = 1; i < this->size(); i++ ) {
        for ( int j = i, prev = j - 1; j > 0 && this->array[j] < this->array[prev]; j--, prev-- ) {
            int temp = this->array[prev];
            
            this->array[prev] = this->array[j];
            this->array[j] = temp;
        }
    }
}

int List::operator[](int index) const {
    return array[index];
}

bool List::operator==(const List& other) const {
    for ( int i = 0; this->array[i] == other.array[i]; i++ ) {
        if ( i == this->current ) {
            return true;
        }
    }
    return false;
}

bool List::operator!=(const List& other) const {
    return !operator==(other);
}

std::ostream& operator<<(std::ostream& out, const List& list) {
    int last = list.size() - 1;

    out << '[';
    for ( int i = 0; i < last; i++ ) {
        out << list[i] << ' ';
    }
    out << list[last] << ']';

    return out;
}
