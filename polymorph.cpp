#include <iostream>

int maximum(int a, int b=100) {
    return ((a > b) ? a : b);
}

char maximum(char a, char b='X') {
    return ((a > b) ? a : b);
}

double maximum(double a, double b=5.343) {
    return ((a > b) ? a : b);
}

int main() {
    // char x;

    // std::cin >> x;

    std::cout << maximum(400) << std::endl;
    std::cout << maximum('D') << std::endl;
    std::cout << maximum(3.555) << std::endl;
    return 0;
}
