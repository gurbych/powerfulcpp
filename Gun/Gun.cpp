#include "Gun.h"

Gun::Gun(const std::string& model, int capacity) {
        this->model = model;
        this->capacity = capacity;
        this->totalShots = 0;
        this->isReady = false;
        this->amount = 0;
}

Gun::~Gun() {}

int Gun::getAmount() const {
        return amount;
}

int Gun::getCapacity() const {
        return capacity;
}

bool Gun::ready() const {
        return isReady;
}

const std::string& Gun::getModel() const {
        return model;
}

int Gun::getTotalShots() const {
        return totalShots;
}

void Gun::prepare() {
        std::cout << " *** " << this->getModel() << " is ready! *** " << '\n';
        isReady = true;
}

void Gun::reload() {
        std::cout << " *** " << this->getModel() << " reloaded! *** " << '\n';
        amount = capacity;
}

void Gun::shoot() {
        if ( !isReady ) {
                throw NotReady();
        }
        if ( amount == 0 ) {
                throw OutOfRounds();
        }
        std::cout << " *** Bang! *** " << std::endl;
        totalShots += 1;
        amount -= 1;
}

std::ostream& operator<<(std::ostream& out, const Gun& gun) {
        std::string state = gun.ready() ? "yes" : "no";
        out << "Model: " << gun.getModel() << '\n';
        out << "Capacity / bullets: " << gun.getCapacity() << " / " << gun.getAmount() << '\n';
        out << "Total shots: " << gun.getModel() << '\n';
        out << "Ready to fire? " << state << '\n';

        return out;
}