#ifndef PASSPORT_H
#define PASSPORT_H

#include <iostream>

class OutOfDateException {};
class InvalidDate_day {};
class InvalidDate_month {};
class InvalidDate_year {};
class FakeIsForbiddenByLaw{};

class Passport {
    private:
        int serial;
        static int total;
        static int lastSerial;
        char firstLetter;
        char secondLetter;
        static char lastFirstLetter;
        static char lastSecondLetter;
        int day;
        int month;
        int year;

        void validate(int day, int month, int year);
    public:
        std::string firstName;
        std::string lastName;
        Passport(const std::string& lastName, const std::string& firstName, int day=1, int month=1, int year=1970);
        Passport(const Passport& other);
        ~Passport();
        static int getTotal();
        int getSerial() const;
        char getFirstLetter() const;
        char getSecondLetter() const;
        static void setID(char a1 = 'A', char a2 = 'A', int newSerial = 100000);
        const std::string& getFirstName() const;
        const std::string& getLastName() const;
        int getDay() const;
        int getMonth() const;
        int getYear() const;
        Passport& operator=(const Passport& copy);
};

std::ostream& operator<<(std::ostream& out, const Passport& passport);

#endif