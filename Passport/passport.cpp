#include "passport.h"
#include <cstring>

int Passport::total = 0;
int Passport::lastSerial = 100000;
char Passport::lastFirstLetter = 'A';
char Passport::lastSecondLetter = 'A';

void Passport::validate(int day, int month, int year) {
    int daysInFebruary = 28;

    if ( year % 4 == 0 ) {
        daysInFebruary += 1;
    }

    if ( year < 1901 || year > 2014 ) {
        throw InvalidDate_year();
    }

    if ( month < 1 || month > 12 ) {
        throw InvalidDate_month();
    }

    if ( month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 ||month == 12) {
            if (day < 1 || day > 31) {
                throw InvalidDate_day();
            }
    }

    if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day < 1 || day > 30) {
                throw InvalidDate_day();
            }
    }

    if (month == 2) {
            if (day < 1 || day > daysInFebruary) {
                throw InvalidDate_day();
            }
    }
}

Passport::Passport(const std::string& lastName, const std::string& firstName,  int day, int month, int year) {
    validate(day, month, year);
    this->day = day;
    this->month = month;
    this->year = year;
    if ( lastSerial == 1000000 ) {
        lastSerial = 100000;
        if ( lastSecondLetter == 'Z' ) {
            firstLetter += 1;
            lastSecondLetter = 'A';
        } else {
            lastSecondLetter += 1;
        }
    }
    this->firstName = firstName;
    this->lastName = lastName;
    this->firstLetter = lastFirstLetter;
    this->secondLetter = lastSecondLetter;

    total += 1;
    this->serial = lastSerial;
    lastSerial += 1;
}

// if someone has a namesake, born on the same day:

Passport::Passport(const Passport& other) {

    if ( lastSerial == 1000000 ) {
        lastSerial = 100000;
        if ( secondLetter == 'Z' ) {
            firstLetter += 1;
            secondLetter = 'A';
        } else {
            secondLetter += 1;
        }
    }
    if ( firstLetter > 'Z' ) {
        throw OutOfDateException();
    }
    this->day = other.day;
    this->month = other.month;
    this->year = other.year;
    this->firstName = other.firstName;
    this->lastName = other.lastName;
    this->firstLetter = lastFirstLetter;
    this->secondLetter = lastSecondLetter;

    total += 1;
    this->serial = lastSerial;
    lastSerial += 1;
}

Passport::~Passport() {
    total -= 1;
}

int Passport::getTotal() {
    std::cout << "Total number of passports: ";
    return total;
}

int Passport::getSerial() const {
    return serial;
}

char Passport::getFirstLetter() const {
    return firstLetter;
}

char Passport::getSecondLetter() const {
    return secondLetter;
}

void Passport::setID(char a1, char a2, int newSerial) {

    if ( a1 < 'A' || a1 > 'Z' ) {
        throw OutOfDateException();
    }
    lastFirstLetter = a1;
    if ( a2 < 'A' || a2 > 'Z' ) {
        throw OutOfDateException();
    }
    lastSecondLetter = a2;
    lastSerial = newSerial;
}

const std::string& Passport::getFirstName() const {
    return firstName;
}

const std::string& Passport::getLastName() const {
    return lastName;
}

int Passport::getDay() const {
    return day;
}

int Passport::getMonth() const {
    return month;
}

int Passport::getYear() const {
    return year;
}

Passport& Passport::operator=(const Passport& copy) {
    throw FakeIsForbiddenByLaw();
}

std::ostream& operator<<(std::ostream& out, const Passport& passport) {
    out << "Series: " << passport.getFirstLetter() << passport.getSecondLetter() << ' ' << passport.getSerial() << '\n';
    out << "Name:   " << passport.getLastName() << ' ' << passport.getFirstName() << '\n';
    out << "DOB:    " << passport.getDay() << '.' << passport.getMonth() << '.' << passport.getYear() << '\n';
    out << "===================================" << std::endl;

    return out;
}