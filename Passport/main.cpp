#include <iostream>
#include "passport.h"

int main() {
    Passport::setID('C', 'M', 828689);

    Passport a1("Gurbych", "Oleksandr", 30, 8, 1989);
    Passport::setID('C', 'M', 795630);
    Passport* a2 = new Passport("Oliinyk", "Dariia", 21, 9, 1989);
    Passport a3(a1);

    std::cout << a1;
    std::cout << *a2;
    //std::cout << a3;

    std::cout << Passport::getTotal() << std::endl;

    delete a2;

    return 0;
}
