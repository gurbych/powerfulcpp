#include <iostream>
#include "Point.h"

int main() {
    Point a(1, 2);
    Point b(3, 4);
  
    std::cout << a.getX() << std::endl;
    std::cout << a.getY() << std::endl;
    std::cout << b.getX() << std::endl;
    std::cout << b.getX() << std::endl;
    a.setX(4);
    std::cout << a.getX() << std::endl;
    std::cout << "distance: " << a.distance(b) << std::endl;

    if (a == b) {
        std::cout << "points are equal" << std::endl;
    }
    if (a != b) {
        std::cout << "points are not equal" << std::endl;
    }

    std::cout << "a: " << a;
    std::cout << "b: " << b;

    return 0;
}
