#include <iostream>

class ArrayIterator {
    private:
        int* array;
        int index;
        int last;
    public:
        ArrayIterator(int* array, int n) {
            this->index = 0;
            this->last = n - 1;
            this->array = array;
        }

        void next() {
            if ( over() ) {
                return;
            }
        index += 1 ;
        }

        void operator++() {
            next();
        }

        void operator++(int) {
            operator++();
        }

        bool over() {
            return index > last;
        }

        char value() {
            return array[index];
        }

        int operator*() {
            return value();
        }

        int operator[](int index) const {
            return array[index];
        }
};

int main() {
    int array[10] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
    ArrayIterator seq(array, 10);

    for ( ; !seq.over(); seq++ ) {
        std::cout << *seq << std::endl;
    }

    return 0;
}
