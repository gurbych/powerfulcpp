#ifndef ITERATOR_H
#define ITERATOR_H

#include <iostream>

class Iterator {
    private:
        int current; // sequence starts from this element
        int step;
        int limit; // number of elements in sequence to show (first element included)
    public:
        Iterator(int current, int step, int limit);
        void next();
        void operator++();
        void operator++(int);
        bool over();
        int value();
        int operator*();
};

#endif