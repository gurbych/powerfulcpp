#include "Iterator.h"

Iterator::Iterator(int current, int step, int limit) {
    this->current = current;
    this->step = step;
    this->limit = (limit - 1) * step + current;
}

void Iterator::next() {
    if ( over() ) {
        return;
    }
    current += step;
}

void Iterator::operator++() {
    next();
}

void Iterator::operator++(int) {
    operator++();
}

bool Iterator::over() {
    return current > limit;
}

int Iterator::value() {
    return current;
}

int Iterator::operator*() {
    return value();
}
