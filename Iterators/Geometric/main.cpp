#include "Iterator.h"

int main() {
    Iterator seq(5, 2, 5);

     for ( ; !seq.over(); seq++ ) {
         std::cout << seq.value() << std::endl;
     }
    // std::cout << seq.value() << " " << seq.over() << std::endl;
    return 0;
}
