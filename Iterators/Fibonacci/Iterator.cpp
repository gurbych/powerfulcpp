#include "Iterator.h"

// fucking hell starts here

Iterator::Iterator(int fib) {
    index = 0;
    prev = 0;
    this->fib = 1;
    sum;
    isNegative = false;
    
    if ( fib > 46 || fib < -46 ) {
        throw OutOfRangeException();
    }
    if ( fib < 0 ) {
        fib *= -1;
        isNegative = true;
    }
    this->range = fib;
    ar[0] = 0;
    ar[1] = 1;
    for ( int i = 2; i <= fib; i++ ) {
        sum = prev + this->fib;
        prev = this->fib;
        this->fib = sum;
        if ( isNegative ) {
            if ( i % 2 == 0 ) {
            sum *= -1;
            }
        }
        ar[i] = sum;
    }
}

void Iterator::next() {
    if ( over() ) {
        return;
    }
    index += 1;
}

void Iterator::operator++() {
    next();
}

void Iterator::operator++(int) {
    operator++();
}

bool Iterator::over() {
    return index > range;
}

int Iterator::value() {
    return ar[index];
}

int Iterator::operator*() {
    return value();
}
