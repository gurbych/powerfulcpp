#ifndef ITERATOR_H
#define ITERATOR_H

#include <iostream>

class OutOfRangeException {};

class Iterator {
    private:
        int range;
        int index;
        int prev;
        int sum;
        bool isNegative;
        int ar[47]; // cash for fibonacci numbers (limited by 46 of them plus 0)
        int fib; // number of elements in sequence to show (first element included)
    public:
        Iterator(int fib);
        void next();
        void operator++();
        void operator++(int);
        bool over();
        int value();
        int operator*();
};

#endif