#include "Iterator.h"

Iterator::Iterator(int fact) {
    this->result = 1;
    this->i = 2;
    this->fact = 1;
    if ( fact < 0 ) {
        throw OutOfRangeException();
    } else if ( fact == 0 ) {
        this->fact = 1;
    }
    for ( int i = 2; i <= fact; i++ ) {
        this->fact *= i;
    }
}

void Iterator::next() {
    if ( over() ) {
        return;
    }
    result *= i;
    i+= 1;
}

void Iterator::operator++() {
    next();
}

void Iterator::operator++(int) {
    operator++();
}

bool Iterator::over() {
    return result > fact;
}

int Iterator::value() {
    return result;
}

int Iterator::operator*() {
    return value();
}
