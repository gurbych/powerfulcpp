#ifndef ITERATOR_H
#define ITERATOR_H

#include <iostream>

class OutOfRangeException {};

class Iterator {
    private:
        int result; // sequence starts from this element
        int i;
        int fact; // number of elements in sequence to show (first element included)
    public:
        Iterator(int fact);
        void next();
        void operator++();
        void operator++(int);
        bool over();
        int value();
        int operator*();
};

#endif