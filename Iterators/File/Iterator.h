#ifndef ITERATOR_H
#define ITERATOR_H

#include <iostream>
#include <fstream>

class NoFileException {};

class Iterator {
    private:
        std::ifstream in;
        std::ofstream out;
        int ar[10000];
        int index;
        int i;
    public:
        Iterator(std::ifstream& in);
        void next();
        void operator++();
        void operator++(int);
        bool over();
        int value(std::ofstream& out);
        int operator*();
};

#endif