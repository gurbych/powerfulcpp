#include "Iterator.h"

int main() {
    std::ifstream in("task.in");
    std::ofstream out("task.out");
    Iterator seq(in);

    for ( ; !seq.over(); seq++ ) {
        std::cout << seq.value(out) << std::endl;
    }

    in.close();
    out.close();

    return 0;
}
