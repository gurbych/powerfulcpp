#include "Iterator.h"

Iterator::Iterator(std::ifstream& in) {
    this->index = 0;
    this->i = 0;
    if ( in ) {
        for ( ; !in.eof(); i++ ) {
            in >> ar[i];
        }
    i -= 1;
    } else {
        throw NoFileException();
    }
}

void Iterator::next() {
    if ( over() ) {
        return;
    }
    index += 1;;
}

void Iterator::operator++() {
    next();
}

void Iterator::operator++(int) {
    operator++();
}

bool Iterator::over() {
    return index > i;
}

int Iterator::value(std::ofstream& out) {
    out << ar[index] << ' ';
    return ar[index];
}

int Iterator::operator*() {
    return value(out);
}
