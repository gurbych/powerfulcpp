#include "Iterator.h"

int main() {
    Iterator seq(10); // amount of prime numbers to be shown

     for ( ; !seq.over(); seq++ ) {
         std::cout << seq.value() << std::endl;
     }

    return 0;
}
