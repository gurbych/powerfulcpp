#ifndef ITERATOR_H
#define ITERATOR_H

#include <iostream>

class OutOfRangeException {};

class Iterator {
    private:
        int primeIndex;
        int currentIndex;
        int ar[100];
        bool isPrime(int i);
    public:
        Iterator(int n);
        void next();
        void operator++();
        void operator++(int);
        bool over();
        int value();
        int operator*();
};

#endif