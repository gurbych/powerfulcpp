#include "Iterator.h"

Iterator::Iterator(int n) {
    if ( n > 100 || n < 1 ) {
        throw OutOfRangeException();
    }
    ar[0] = 2;
    ar[1] = 3;
    this->primeIndex = n-1;
    this->currentIndex = 0;
    for ( int i = 5, counter = 2; counter <= n; i += 2 ) {
        if ( isPrime(i) ) {
            ar[counter] = i;
            counter += 1;
        }
    }
}

bool Iterator::isPrime(int i) {
    for ( int counter = 2; counter < i; counter++ ) {
        if ( i % counter == 0 ) {
            return false;
        }
    }
    return true;
}

void Iterator::next() {
    if ( over() ) {
        return;
    }
    currentIndex += 1;
}

void Iterator::operator++() {
    next();
}

void Iterator::operator++(int) {
    operator++();
}

bool Iterator::over() {
    return currentIndex > primeIndex;
}

int Iterator::value() {
    return ar[currentIndex];
}

int Iterator::operator*() {
    return value();
}
