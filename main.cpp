#include <iostream>
#include "vectorTest.h"

using namespace std;

int main() {
    Point a(1, 2);
    Point b(3, 4);

    //a.setX(42);
    cout << "a: " << a;
    cout << "b: " << b << endl;
    cout << "b.x: " << b.getX() << endl;
    cout << "b.y: " << b.getY() << endl;
    cout << "a.x: " << a.getX() << endl;
    cout << "a.y: " << a.getY() << endl;
    cout << "distance: " << a.distance(b) << endl;
    if (a == b) {
        cout << "points are equal";
    }
    if (a != b) {
        cout << "points are not equal";
    }

    return 0;
}
