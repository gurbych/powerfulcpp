#include "Pen.h"

Pen::Pen(int inkCapacity) {
    this->inkCapacity = inkCapacity;
    this->inkAmount = inkCapacity;
}

Pen::~Pen() {}

int Pen::getInkAmount() const {
    return inkAmount;
}

int Pen::getInkCapacity() const {
    return inkCapacity;
}

void Pen::write(Paper& paper, const std::string& message) {
    int newInkAmount = inkAmount - message.length();

    if ( newInkAmount < 0 ) {
        paper.addContent(message.substr(0, inkAmount));
        throw OutOfInk();
    } else {
        paper.addContent(message);
    }
}

void Pen::refill() {
    inkAmount = inkCapacity;
}