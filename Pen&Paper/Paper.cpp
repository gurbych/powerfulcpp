#include "Paper.h"
#include <cstring>

Paper::Paper(int maxSymbols) {
    this->maxSymbols = maxSymbols;
    this->symbols = 0;
}

Paper::~Paper() {}

int Paper::getMaxSymbols() const {
    return maxSymbols;
}

int Paper::getSymbols() const {
    return symbols;
}

void Paper::addContent(const std::string& message) {
    int freeSpace = maxSymbols - symbols;
    int length = message.length();

    if ( freeSpace < length ) {
        content += message.substr(0, freeSpace);
        symbols += freeSpace;
        throw OutOfSpace();
    } else {
        content += message;
        symbols += length;
    }
}

void Paper::show() const {
    std::cout << content << std::endl;
}