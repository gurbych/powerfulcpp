#include <iostream>
#include "Complex.h"

int main() {
    Complex a(1, 1);
    Complex b(3, 4);

    std::cout << a;
    std::cout << b;
    std::cout << (a == b) << std::endl;
    std::cout << (a != b) << std::endl;
    a += b;
    std::cout << a;
    a -= b;
    std::cout << a;
    std::cout << (a + b);
    std::cout << (b - a);
    std::cout << a * b;

    return 0;
}
