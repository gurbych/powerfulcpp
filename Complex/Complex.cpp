#include "Complex.h"

Complex::Complex(double real, double imaginary) {
        this->real = real;
        this->imaginary = imaginary;
}

Complex::~Complex() {}

double Complex::getReal() const {
        return real;
}

double Complex::getImaginary() const {
        return imaginary;
}

bool Complex::operator==(const Complex& other) const {
        return this->real == other.real && this->imaginary == other.imaginary;
}

bool Complex::operator!=(const Complex& other) const {
        return this->real != other.real && this->imaginary != other.imaginary;
}

void Complex::operator+=(const Complex& other) {
        this->real += other.real;
        this->imaginary += other.imaginary;
}

void Complex::operator-=(const Complex& other) {
        this->real -= other.real;
        this->imaginary -= other.imaginary;
}

Complex Complex::operator+(const Complex& other) const {
        Complex sum;

        sum.real = this->real + other.real;
        sum.imaginary = this->imaginary + other.imaginary;
        return sum;
}

Complex Complex::operator-(const Complex& other) const {
        Complex diff;

        diff.real = this->real - other.real;
        diff.imaginary = this->imaginary - other.imaginary;
        return diff;
}

Complex Complex::operator*(const Complex& other) const {
        Complex mult;

        mult.real = this->real * other.real - this->imaginary * other.imaginary;
        mult.imaginary = this->real * other.imaginary + this->imaginary * other.real;
        return mult;
}

std::ostream& operator<<(std::ostream& out, const Complex& complex) {
        out << complex.getReal();
        if ( complex.getImaginary() >= 0 ) { 
                out << "+"; 
        }
        out << complex.getImaginary() << "i" << std::endl;
}