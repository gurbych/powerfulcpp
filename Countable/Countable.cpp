#include <iostream>

class Countable{
    private:
        int x, y;
        static int total;
    public:
        Countable () {
            total += 1;
        }

        Countable(const Countable& other) {
            total += 1;
        }

        ~Countable() {
            total -= 1;
        }

        static int getTotal() {
            return total;
        }

};

int Countable::total = 0;

int main() {
    Countable a;
    Countable b; // use constructor
    Countable c = a; // use copy constructor

    std::cout << Countable::getTotal(); // inspect total quantity of instances

    return 0;
}
