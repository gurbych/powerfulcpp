#include "Vector.h"
#include <iostream>

int main() {
    Vector a(1, 1);
    Vector b(3, 4);

    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << (a == b) << std::endl;
    std::cout << (a != b)<< std::endl;
    a += b;
    std::cout << a << std::endl;
    a -= b;
    std::cout << a << std::endl;
    std::cout << (a + b) << std::endl;
    std::cout << (a - b) << std::endl;

    return 0;
}