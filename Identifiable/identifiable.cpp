#include <iostream>

class Identifiable {
    private:
        int serial;
        static int total;
        static int lastSerial;
    public:
        Identifiable() {
            total += 1;
            serial = lastSerial;
            lastSerial += 1;
        }

        int getSerial() {
            return serial;
        }

        ~Identifiable() {
            total -= 1;
        }

        Identifiable(const Identifiable& other) {
            total += 1;
            serial = lastSerial;
            lastSerial += 1;
        }

        static int getTotal() {
            return total;
        }
};

int Identifiable::total = 0;
int Identifiable::lastSerial = 100500;

int main() {
    Identifiable* a = new Identifiable;
    Identifiable* b = new Identifiable;
    Identifiable c = *a;

    std::cout << a->getSerial() << std::endl;
    std::cout << b->getSerial() << std::endl;
    std::cout << c.getSerial() << std::endl;
    std::cout << Identifiable::getTotal() << std::endl;

    delete a;
    delete b;

    return 0;
}
