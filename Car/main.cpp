#include "Car.h"
#include <iostream>

int main() {
    Car car;

    std::cout << car << std::endl;

    car.refill(60);
    car.drive(Point(1, 1));

    std::cout << car << std::endl;

    car.drive(5, 5);

    std::cout << car << std::endl;

    return 0;
}
