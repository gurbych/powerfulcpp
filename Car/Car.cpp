#include "Car.h"

Car::Car(double capacity, double consumption, const Point& location, const std::string& model) {
        this->fuelCapacity = capacity;
        this->fuelConsumption = consumption;
        this->location = location;
        this->model = model;
        this->fuelAmount = 0;
}

Car::~Car() {}

double Car::getFuelAmount() const {
        return fuelAmount;
}

double Car::getFuelCapacity() const {
        return fuelCapacity;
}

double Car::getFuelConsumption() const {
        return fuelConsumption;
}

const Point& Car::getLocation() const {
        return location;
}

const std::string& Car::getModel() const {
        return model;
}

void Car::drive(const Point& destination) {
        double distance = this->location.distance(destination);
        double newFuel = fuelAmount - fuelConsumption * distance;

        if ( newFuel < 0 ) {
                throw OutOfFuel();
        }

        this->fuelAmount = newFuel;
        this->location = destination;
}

void Car::drive(double x, double y) {
        double distance = hypot(this->location.getX() - x, this->location.getY() - y);
        double newFuel = fuelAmount - fuelConsumption * distance;

        if ( newFuel < 0 ) {
                throw OutOfFuel();
        }

        this->fuelAmount = newFuel;
        this->location.setX(x);
        this->location.setY(y);
}

void Car::refill(double fuel) {
        double newFuel = this->fuelAmount + fuel;

        if ( newFuel > fuelCapacity ) {
                throw TooMuchFuel();
        }
        if ( fuel <= 0 ) {
                throw NoFuelFilled();
        }

        this->fuelAmount = newFuel;
}

std::ostream& operator<<(std::ostream& out, const Car& car) {
        out << "Car model:               " << car.getModel() << '\n';
        out << "Fuel capacity / amount:  " << car.getFuelCapacity() << '/' << car.getFuelAmount() << '\n';
        out << "Fuel consumption:        " << car.getFuelConsumption() << '\n';
        out << "Current position:        " << car.getLocation() << '\n';

        return out;
}