#include "Date.h"
#include <iostream>

int main() {
    Date date(30, 12, 1901);

    std::cout << date << std::endl;

    return 0;
}
