#include "Date.h"

void Date::validate(int day, int month, int year) {
    int daysInFebruary = 28;

    if ( year % 4 == 0 ) {
        daysInFebruary += 1;
    }

    if ( year < 1901 || year > 2014 ) {
        throw InvalidDate("Incorrect year");
    }

    if ( month < 1 || month > 12 ) {
        throw InvalidDate("Incorrect month");
    }

    if ( month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 ||month == 12) {
            if (day < 1 || day > 31) {
                throw InvalidDate("Incorrect day");
            }
    }

    if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day < 1 || day > 30) {
                throw InvalidDate("Incorrect day");
            }
    }

    if (month == 2) {
            if (day < 1 || day > daysInFebruary) {
                throw InvalidDate("Incorrect day");
            }
    }
}

Date::Date(int day, int month, int year) {
    try {
        validate(day, month, year);
    } catch ( InvalidDate type ) {
        std::cout << "Invalid Date! " << type.text << std::endl;
    }
    this->day = day;
    this->month = month;
    this->year = year;
}

Date::~Date() {}

int Date::getDay() const {
    return day;
}

int Date::getMonth() const {
    return month;
}

int Date::getYear() const {
    return year;
}

std::ostream& operator<<(std::ostream& out, const Date& date) {
    out << date.getDay() << '.' << date.getMonth() << '.' << date.getYear();

    return out;
}