#include <iostream>

class Mohican {
    private:
        static Mohican* last;
        Mohican* next;
        Mohican* prev;
    public:
        std::string name;
        Mohican(const std::string& text) {
            name = text;
            if ( last == NULL ) {
                last = this;
                prev = NULL;
                next = NULL;
            } else {
                prev = last;
                last->next = this;
                last = this;
                next = NULL;
            }
        }
        ~Mohican() {
             if ( next == NULL ) {
                last = prev;
                prev->next = NULL;
             } else {
                prev->next = next;
                next->prev = prev;
             }
        }
        static const Mohican& getLast() {
            return *last;
        }
};

Mohican* Mohican::last = NULL;

int main() {
    Mohican* m1 = new Mohican("1");
    Mohican* m2 = new Mohican("2");
    Mohican* m3 = new Mohican("3");
    Mohican* m4 = new Mohican("4");
    Mohican* m5 = new Mohican("5");

    delete m3;
    delete m5;
    delete m4;

    std::cout << "Last mohican: " << Mohican::getLast().name << std::endl;

    return 0;
}
