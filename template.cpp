#include <iostream>

template <typename T>
int diff (T a, T b) {
    return a - b;
}

template <typename T>
int mult (T a, T b) {
    return a * b;
}

template <typename T>
int div (T a, T b) {
    return a / b + a % b;
}

template <typename T>
int sum (T a, T b) {
    return a + b;
}

//void foo(int &x) {
//    x += 10;
//}

void foo(int *x) {
    x += 10;
}

int main() {
    //char x = 'Z';
    //char y = 'A';
    int *z = 10;

    //std::cout << sum(x, y) << std::endl;
    //std::cout << diff(x, y) << std::endl;
    //std::cout << div(x, y) << std::endl;
    //std::cout << mult(x, y) << std::endl;
    foo(&z);
    std::cout << z << std::endl;

    return 0;
}
