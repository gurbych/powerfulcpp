#include "Unit.h"

void Unit::ensureIsAlive() {
        if ( hitPoints <= 0 ) {
                throw UnitIsDead();
        }
}

Unit::Unit(const std::string& name, int hp, int dmg) {
        if ( hp < 0 ) {
                hp *= -1;
                std::cout << "Hit Points could not be negative. Fixed." << std::endl;
        }
        ensureIsAlive();
        if ( dmg < 0 ) {
                dmg *= -1;
                std::cout << "Damage could not be negative. Fixed." << std::endl;
        }
        this->name = name;
        this->hitPoints = hp;
        this->damage = dmg;
        this->hitPointsLimit = hp;
}

Unit::~Unit() {}

int Unit::getDamage() const {
        return damage;
}

int Unit::getHitPoints() const {
        return hitPoints;
}

int Unit::getHitPointsLimit() const {
        return hitPointsLimit;
}

const std::string& Unit::getName() const {
        return name;
}

void Unit::addHitPoints(int hp) {
        ensureIsAlive();
        int newHp = hitPoints + hp;

        if ( newHp > hitPointsLimit ) {
                hitPoints = hitPointsLimit;
        } else {
                hitPoints = newHp;
        }
}

void Unit::takeDamage(int dmg) {
        ensureIsAlive();
        hitPoints -= dmg;
}

void Unit::attack(Unit& enemy) {
        std::cout << " *** " << this->getName() << " attacks! *** " << std::endl;
        enemy.takeDamage(this->damage);
        enemy.ensureIsAlive();
        takeDamage(0.5 * enemy.getDamage());
}

void Unit::counterAttack(Unit& enemy) {
        enemy.takeDamage(this->damage * 0.5);
}

std::ostream& operator<<(std::ostream& out, const Unit& unit) {
        out << "Character:           " << unit.getName() << '\n';
        out << "current HP / max HP: " << unit.getHitPoints() << " / " << unit.getHitPointsLimit() << '\n';
        out << "Damage:              " << unit.getDamage() << std::endl;

        return out;
}