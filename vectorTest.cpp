#include "vectorTest.h"

class Point {
    private:
        int x, y;
    public:
        Point (int x, int y) {
            this->x = x;
            this->y = y;
        }

        int getX() const {
            return x;
        }

        int getY() const {
            return y;
        }

        void setX(int x) {
            if ( x > 0) {
                this->x = x;
            }
        }

        void setY(int y) {
            this->y = y;
        }

        bool operator==(const Point& other) const {
            return this->x == other.x && this->y == other.y;
        }

        bool operator!=(const Point& other) const {
           return !operator==(other);
        }

        void operator+=(const Point& other) {
           this->x += other.x;
           this->y += other.y;
        }

        void operator-=(const Point& other) {
            this->x -= other.x;
            this->y -= other.y;
        }

        Point operator+(const Point& other) const {
            Point sum = *this;
            sum += other;
            return sum;
        }

        Point operator-(const Point& other) const {
            Point diff = *this;
            diff -= other;
            return diff;
        }

        int distance (const Point& other) const {
            return hypot(this->x - other.x, this->y - other.y);
        }
};

ostream& operator << (std::ostream& printer, const Point& point) {
    printer << "(" << point.getX() << ", " << point.getY() << ")" << endl;
    return printer;
}
